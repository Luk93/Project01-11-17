function [images, labels] =Bilanciamento(images,labels)
contatore=zeros(1,10);
for i=1:length(labels)
    contatore(labels(i)+1)=contatore(labels(i)+1)+1;
end
massimo=max(contatore);
for i=1:10
    mancanti=massimo-contatore(i);
    if(mancanti>0)
         temp=find(i-1==labels);
         k=randperm(length(temp));
         while(mancanti>0)
         images(:,:,1,length(labels)+1)=images(:,:,1,temp(k(mod(mancanti,length(temp))+1)));%modulo per ripetere piu volte il campione
         labels(length(labels)+1)=labels(temp(k(mod(mancanti,length(temp))+1)));
         mancanti=mancanti-1;
         end
    end
        
        
end
end
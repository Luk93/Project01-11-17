function confusionMatrix(label,labelNet,text)
figure; hold on
temp1=zeros(10,numel(label));
temp2=zeros(10,numel(labelNet));
label=categorical(label);
for i=1:numel(label)
    temp1(label(i),i)=1;
    temp2(labelNet(i),i)=1;
end
plotconfusion(temp1,temp2,text);
end
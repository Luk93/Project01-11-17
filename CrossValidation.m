

reg=0.0005;
epoch=20;
minibatch=100;
momentum=0.9;
dim=length(labels);
passo=5;
acc=[];
val=[];


ErrorVa1=zeros(1,passo);
ErrorTr1=zeros(1,passo);

l1=imageInputLayer([28 28 1]);
l2=convolution2dLayer(5,1,'Stride',1);
l3=reluLayer;
l4=maxPooling2dLayer(2,'Stride',2);
l5=convolution2dLayer(5,20,'Stride',1);
l6=reluLayer;
l7=maxPooling2dLayer(2,'Stride',2);
l8=fullyConnectedLayer(40);
l9=fullyConnectedLayer(150);
l10=fullyConnectedLayer(10);
l11=softmaxLayer;
l12=classificationLayer();
rng(1,'twister');
l2.Bias=randn([1 1 1])*0.01;
l2.Weights=randn([5 5 1 1]) * 0.01;

l5.Bias=randn([1 1 20])*0.01;
l5.Weights=randn([5 5 1 20]) * 0.01;

l8.Bias=randn([40 1])*0.01;
l8.Weights=randn([40 320]) * 0.01;

l9.Bias=randn([150 1])*0.01;
l9.Weights= randn([150 40]) * 0.01;

l10.Bias= randn([10 1]) * 0.01;
l10.Weights=randn([10 150]) * 0.01;
CVO = cvpartition(labels,'KFold',passo);

for i=1:CVO.NumTestSets
layers = [l1
    l2
    l3
    l4
    l5
    l6
    l7
    l8
    l9
    l10
    l11
    l12];
options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.01,...
          'LearnRateSchedule','piecewise',...
          'LearnRateDropFactor',0.9993,...
          'LearnRateDropPeriod',1,... 
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
  [convnet,traininfo]  = trainNetwork(images(:,:,1,CVO.training(i)),categorical(labels(CVO.training(i))),layers,options);
  acc= cat(2,acc,traininfo.TrainingAccuracy);
  YVal = classify(convnet,images(:,:,1,CVO.test(i)),'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
  val=cat(2,val,categorical(YVal) ~= categorical(labels(CVO.test(i))));
  ErrorVa1(i) = (sum(val(1:end)))/numel(val)*100;
ErrorTr1(i)=(sum(100-acc(1:end))/(numel(acc)));
l1=convnet.Layers(1);
l2= convnet.Layers(2);
l3=convnet.Layers(3);
l4=convnet.Layers(4);
l5= convnet.Layers(5);
l6=convnet.Layers(6);
l7=convnet.Layers(7);
l8=convnet.Layers(8);
l9= convnet.Layers(9);
l10=convnet.Layers(10);
l11= convnet.Layers(11);
l12=convnet.Layers(12);
end

figure
plot(1:CVO.NumTestSets,ErrorTr1,'b-',1:CVO.NumTestSets,ErrorVa1,'g-')
title('Misclassification1')
xlabel('passo')
ylabel('Error(%)')
legend('training error','validation error')
Error1=ErrorVa1(end);
%-------------------------------------------
ErrorVa2=zeros(1,passo);
ErrorTr2=zeros(1,passo);
l1=imageInputLayer([28 28 1]);
l2=convolution2dLayer(5,20,'Stride',1);
          l3=reluLayer;
          l4=maxPooling2dLayer(2,'Stride',2);
          l5=convolution2dLayer(5,40,'Stride',1);
          l6=reluLayer;
          l7=maxPooling2dLayer(2,'Stride',2);
          l8=fullyConnectedLayer(640);
          l9=reluLayer;
          l10=dropoutLayer();
          l11=fullyConnectedLayer(1000);
          l12=reluLayer;
          l13=dropoutLayer();
          l14=fullyConnectedLayer(10);
          l15=softmaxLayer;
          l16=classificationLayer();
rng(1,'twister');
l2.Weights = (randn([5 5 1 20]) * 0.01);
l2.Bias = (randn([1 1 20])*0.01);
l5.Weights = (randn([5 5 20 40]) * 0.01);
l5.Bias = (randn([1 1 40])*0.01);
l8.Weights = (randn([640 640]) * 0.01);
l8.Bias = (randn([640 1])*0.01);
l11.Weights = (randn([1000 640]) * 0.01);
l11.Bias = (randn([1000 1])*0.01);
l14.Weights = (randn([10 1000]) * 0.01);
l14.Bias = (randn([10 1])*0.01);
CVO = cvpartition(labels,'KFold',passo);
for i=1:CVO.NumTestSets
layers = [l1
    l2
    l3
    l4
    l5
    l6
    l7
    l8
    l9
    l10
    l11
    l12
    l13
    l14
    l15
    l16];
options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.01,...
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
  [convnet,traininfo]  = trainNetwork(images(:,:,1,CVO.training(i)),categorical(labels(CVO.training(i))),layers,options);
  acc= cat(2,acc,traininfo.TrainingAccuracy);
  YVal = classify(convnet,images(:,:,1,CVO.test(i)),'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
  val=cat(2,val,categorical(YVal) ~= categorical(labels(CVO.test(i))));
  ErrorVa2(i) = (sum(val(1:end)))/numel(val)*100;
ErrorTr2(i)=(sum(100-acc(1:end))/(numel(acc)));
l1=convnet.Layers(1);
l2= convnet.Layers(2);
l3=convnet.Layers(3);
l4=convnet.Layers(4);
l5= convnet.Layers(5);
l6=convnet.Layers(6);
l7=convnet.Layers(7);
l8=convnet.Layers(8);
l9= convnet.Layers(9);
l10=convnet.Layers(10);
l11= convnet.Layers(11);
l12=convnet.Layers(12);
l13=convnet.Layers(13);
l14=convnet.Layers(14);
l15= convnet.Layers(15);
l16=convnet.Layers(16);
end

figure
plot(1:CVO.NumTestSets,ErrorTr2,'b-',1:CVO.NumTestSets,ErrorVa2,'g-')
title('Misclassification2')
xlabel('passo')
ylabel('Error(%)')
legend('training error','validation error')
Error2=ErrorVa2(end);
%----------------------------------------------------------------
%}
ErrorVa3=zeros(1,passo);
ErrorTr3=zeros(1,passo);
l1=imageInputLayer([28 28 1]);
l2=convolution2dLayer(5,20,'Stride',1);
l3=maxPooling2dLayer(2,'Stride',2);
l4=convolution2dLayer(5,50,'Stride',1);
l5=maxPooling2dLayer(2,'Stride',2);
l6=fullyConnectedLayer(500);
l7=reluLayer;
l8=fullyConnectedLayer(10);
l9=softmaxLayer;
l10=classificationLayer();
rng(1,'twister');
l2.Weights = (randn([5 5 1 20]) * 0.01);
l2.Bias = (randn([1 1 20])*0.01);
l4.Weights = (randn([5 5 20 50]) * 0.01);
l4.Bias = (randn([1 1 50])*0.01);
l6.Weights = (randn([500 800]) * 0.01);
l6.Bias = (randn([500 1])*0.01);
l8.Weights =( randn([10 500]) * 0.01);
l8.Bias = (randn([10 1])*0.01); 

CVO = cvpartition(labels,'KFold',passo);
for i=1:CVO.NumTestSets
layers = [l1
    l2
    l3
    l4
    l5
    l6
    l7
    l8
    l9
    l10];
options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.001,...
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
  [convnet,traininfo]  = trainNetwork(images(:,:,1,CVO.training(i)),categorical(labels(CVO.training(i))),layers,options);
  acc= cat(2,acc,traininfo.TrainingAccuracy);
  YVal = classify(convnet,images(:,:,1,CVO.test(i)),'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
  val=cat(2,val,categorical(YVal) ~= categorical(labels(CVO.test(i))));
  ErrorVa3(i) = (sum(val(1:end)))/numel(val)*100;
ErrorTr3(i)=(sum(100-acc(1:end))/(numel(acc)));
l1=convnet.Layers(1);
l2= convnet.Layers(2);
l3=convnet.Layers(3);
l4=convnet.Layers(4);
l5= convnet.Layers(5);
l6=convnet.Layers(6);
l7=convnet.Layers(7);
l8=convnet.Layers(8);
l9= convnet.Layers(9);
l10=convnet.Layers(10);
end

figure
plot(1:CVO.NumTestSets,ErrorTr3,'b-',1:CVO.NumTestSets,ErrorVa3,'g-')
title('Misclassification3')
xlabel('passo')
ylabel('Error(%)')
legend('training error','validation error')
Error3=ErrorVa3(end);
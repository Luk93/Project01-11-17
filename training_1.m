rng(1,'twister');
reg=0.00005;
epoch=20;
minibatch=100;
momentum=0.9;
dim= size(imtr,4);
pazienza=5;
ErrorVa=zeros(1,epoch);
ErrorTr=zeros(1,epoch);
ErrorTot=zeros(1,epoch);

Network(1).Weights = (randn([5 5 1 1]) * 0.01);
Network(1).Bias = (randn([1 1 1])*0.01);
Network(2).Weights = (randn([5 5 1 20]) * 0.01);
Network(2).Bias = (randn([1 1 20])*0.01);
Network(3).Weights = (randn([40 320]) * 0.01);
Network(3).Bias = (randn([40 1])*0.01);
Network(4).Weights =( randn([150 40]) * 0.01);
Network(4).Bias = (randn([150 1])*0.01);
Network(5).Weights =( randn([10 150]) * 0.01);
Network(5).Bias = (randn([10 1])*0.01);

%elimina le reti nella directory Model1
flist=dir('.\Model1\*.mat');
for ii=1:length(flist)
    
     delete(strcat(flist(ii).folder,'\',flist(ii).name));
end

layers = [imageInputLayer([28 28 1])
          convolution2dLayer(5,1,'Stride',1)
          reluLayer
          maxPooling2dLayer(2,'Stride',2)
          convolution2dLayer(5,20,'Stride',1)
          reluLayer
          maxPooling2dLayer(2,'Stride',2)
          fullyConnectedLayer(40)
          reluLayer
          fullyConnectedLayer(70)
          reluLayer
          fullyConnectedLayer(10)
          softmaxLayer
          classificationLayer()];
%pesi struttura modificata
Network(4).Weights =( randn([70 40]) * 0.01);
Network(4).Bias = (randn([70 1])*0.01);
Network(5).Weights =( randn([10 70]) * 0.01);
Network(5).Bias = (randn([10 1])*0.01);
%assegno i pesi
layers(2).Weights = Network(1).Weights;
layers(2).Bias = Network(1).Bias;
layers(5).Weights = Network(2).Weights;
layers(5).Bias = Network(2).Bias;
layers(8).Weights = Network(3).Weights;
layers(8).Bias = Network(3).Bias;
layers(10).Weights =Network(4).Weights;
layers(10).Bias = Network(4).Bias;
layers(12).Weights =Network(5).Weights;
layers(12).Bias = Network(5).Bias;
       options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'CheckpointPath','.\Model1',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.01,...
          'LearnRateSchedule','piecewise',...
          'LearnRateDropFactor',0.9993,... 
          'LearnRateDropPeriod',1,... 
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
  [convnet,traininfo]  = trainNetwork(imtr,categorical(labelstra),layers,options);
  %legge tutte le reti nella directory Model1
flist=dir('.\Model1\*.mat');
ii = [flist(:).datenum];
[~,ii] = sort(ii);
flist = {flist(ii).name};
Errtemp=100;
netprec=[];
for i=1:length(flist)
     network=load(strcat('.\Model1\',string(flist(i)))); 
     YVal = classify(network.net,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
     TVal = (categorical(labelsval));
ErrorVa(i) = (sum(categorical(YVal) ~= categorical(TVal))/numel(TVal))*100;
ErrorTr(i)=(sum(100-traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))))/(numel(traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))));
%netprec =network.net;

if(mod(i,pazienza)==0)%earlystopping
if(Errtemp>ErrorVa(i))
    Errtemp=ErrorVa(i);
    netprec =network.net;
else 
    i=i-pazienza;
    break;

end

end
%}
end
figure
plot(pazienza:pazienza:i,traininfo.TrainingLoss(pazienza*fix(dim/minibatch):pazienza*fix(dim/minibatch):i*fix(dim/minibatch)),'c-')
title('Model 1 Training Loss function')
xlabel('Epoch')
ylabel('Loss')

figure
plot(pazienza:pazienza:i,ErrorTr(pazienza:pazienza:i),'b-',pazienza:pazienza:i,ErrorVa(pazienza:pazienza:i),'g-')
title('Model 1')
xlabel('Epoch')
ylabel('Error(%)')
legend('training error','validation error')

YVal = classify(netprec,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
confusionMatrix(labelsval,YVal,'Model 1 Validation set');%il primo argomento indica la vera classe il secondo indicano le predizioni della rete neurale

figure;
perm = randperm(length(YVal));
for i = 1:20
    subplot(4,5,i);
    imshow(imval(:,:,1,perm(i)),[0 255]);
    title(char(YVal(perm(i))));
end



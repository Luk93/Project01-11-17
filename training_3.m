rng(1,'twister');
reg=0.005;
epoch=20;
minibatch=100;
momentum=0.9;
dim= size(imtr,4);
pazienza=5;
ErrorVa=zeros(1,epoch);
ErrorTr=zeros(1,epoch);
ErrorTot=zeros(1,epoch);
%pesi della struttura iniziale
NetworkWB(1).Weights = (randn([5 5 1 20]) * 0.01);
NetworkWB(1).Bias = (randn([1 1 20])*0.01);
NetworkWB(2).Weights = (randn([5 5 20 50]) * 0.01);
NetworkWB(2).Bias = (randn([1 1 50])*0.01);
NetworkWB(3).Weights = (randn([500 800]) * 0.01);
NetworkWB(3).Bias = (randn([500 1])*0.01);
NetworkWB(4).Weights =( randn([10 500]) * 0.01);
NetworkWB(4).Bias = (randn([10 1])*0.01); 

%elimina le reti nella directory Model3
flist=dir('.\Model3\*.mat');
 for ii=1:length(flist)
     delete(strcat(flist(ii).folder,'\',flist(ii).name));
 end


layers = [imageInputLayer([28 28 1])
    convolution2dLayer(5,20,'Stride',1)
    maxPooling2dLayer(2,'Stride',2);
    convolution2dLayer(5,25,'Stride',1)
    maxPooling2dLayer(2,'Stride',2)
    fullyConnectedLayer(300)
    reluLayer
    fullyConnectedLayer(10)
    softmaxLayer
    classificationLayer()];
%pesi struttura modificata
NetworkWB(2).Weights = (randn([5 5 20 25]) * 0.01);
NetworkWB(2).Bias = (randn([1 1 25])*0.01);
NetworkWB(3).Weights = (randn([300 400]) * 0.01);
NetworkWB(3).Bias = (randn([300 1])*0.01);
NetworkWB(4).Weights =( randn([10 300]) * 0.01);
NetworkWB(4).Bias = (randn([10 1])*0.01); 

%assegno i pesi
layers(2).Weights = NetworkWB(1).Weights;
layers(2).Bias = NetworkWB(1).Bias;
layers(4).Weights = NetworkWB(2).Weights;
layers(4).Bias = NetworkWB(2).Bias;
layers(6).Weights = NetworkWB(3).Weights;
layers(6).Bias = NetworkWB(3).Bias;
layers(8).Weights =NetworkWB(4).Weights;
layers(8).Bias = NetworkWB(4).Bias;

      options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'CheckpointPath','.\Model3',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.001,...
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
[convnet,traininfo]  = trainNetwork(imtr,categorical(labelstra),layers,options);
%legge tutte le reti nella directory Model3
flist=dir('.\Model3\*.mat');
ii = [flist(:).datenum]; 
[~,ii] = sort(ii);
flist = {flist(ii).name};
Errtemp=100;
netprec=[];
for i=1:length(flist)
     network=load(strcat('.\Model3\',string(flist(i))));  
     YVal = classify(network.net,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
     TVal = (categorical(labelsval));
ErrorVa(i) = (sum(categorical(YVal) ~= categorical(TVal))/numel(TVal))*100;
ErrorTr(i)=(sum(100-traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))))/(numel(traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))));
%netprec =network.net;

if(mod(i,pazienza)==0)%earlystopping
if(Errtemp>ErrorVa(i))
    Errtemp=ErrorVa(i);
    netprec =network.net;
else 
    i=i-pazienza;
    break;

end

end
%}
end
figure
plot(pazienza:pazienza:i,traininfo.TrainingLoss(pazienza*fix(dim/minibatch):pazienza*fix(dim/minibatch):i*fix(dim/minibatch)),'c-')
title('Model 3 Loss function')
xlabel('Epoch')
ylabel('Loss')

figure
plot(pazienza:pazienza:i,ErrorTr(pazienza:pazienza:i),'b-',pazienza:pazienza:i,ErrorVa(pazienza:pazienza:i),'g-')
title('Model 3')
xlabel('Epoch')
ylabel('Error(%)')
legend('training error','validation error')

YVal = classify(netprec,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
confusionMatrix(labelsval,YVal,'Model 3 Validation set');

figure;
perm = randperm(length(YVal));
for i = 1:20
    subplot(4,5,i);
    imshow(imval(:,:,1,perm(i)),[0 255]);
    title(char(YVal(perm(i))));
end

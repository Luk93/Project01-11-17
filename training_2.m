rng(1,'twister');
reg=0.0005;
epoch=20;
minibatch=100;
momentum=0.9;
dim= size(imtr,4);
pazienza=5;
ErrorVa=zeros(1,epoch);
ErrorTr=zeros(1,epoch);
ErrorTot=zeros(1,epoch);
%pesi della struttura iniziale
NetworkWB(1).Weights = (randn([5 5 1 20]) * 0.01);
NetworkWB(1).Bias = (randn([1 1 20])*0.01);
NetworkWB(2).Weights = (randn([5 5 20 40]) * 0.01);
NetworkWB(2).Bias = (randn([1 1 40])*0.01);
NetworkWB(3).Weights = (randn([640 640]) * 0.01);
NetworkWB(3).Bias = (randn([640 1])*0.01);
NetworkWB(4).Weights = (randn([1000 640]) * 0.01);
NetworkWB(4).Bias = (randn([1000 1])*0.01);
NetworkWB(5).Weights = (randn([10 1000]) * 0.01);
NetworkWB(5).Bias = (randn([10 1])*0.01);

%elimina le reti nella directory Model2
flist=dir('.\Model2\*.mat');
 for ii=1:length(flist)
     delete(strcat(flist(ii).folder,'\',flist(ii).name));
 end
  
layers = [imageInputLayer([28 28 1])
          convolution2dLayer(5,60,'Stride',1)
          reluLayer
          maxPooling2dLayer(2,'Stride',2)
          convolution2dLayer(5,100,'Stride',1)
          reluLayer
          maxPooling2dLayer(2,'Stride',2)
          fullyConnectedLayer(640)
          reluLayer
          dropoutLayer()
          fullyConnectedLayer(1000)
          reluLayer
          dropoutLayer()
          fullyConnectedLayer(10)
          softmaxLayer
          classificationLayer()];
      
%pesi struttura modificata
NetworkWB(1).Weights = (randn([5 5 1 60]) * 0.01);
NetworkWB(1).Bias = (randn([1 1 60])*0.01);
NetworkWB(2).Weights = (randn([5 5 60 100]) * 0.01);
NetworkWB(2).Bias = (randn([1 1 100])*0.01);
NetworkWB(3).Weights = (randn([640 1600]) * 0.01);
NetworkWB(3).Bias = (randn([640 1])*0.01);
NetworkWB(4).Weights = (randn([1000 640]) * 0.01);
NetworkWB(4).Bias = (randn([1000 1])*0.01);
%assegno i pesi
layers(2).Weights = NetworkWB(1).Weights;
layers(2).Bias = NetworkWB(1).Bias;
layers(5).Weights = NetworkWB(2).Weights;
layers(5).Bias = NetworkWB(2).Bias;
layers(8).Weights = NetworkWB(3).Weights;
layers(8).Bias = NetworkWB(3).Bias;
layers(11).Weights =NetworkWB(4).Weights;
layers(11).Bias = NetworkWB(4).Bias;
layers(14).Weights =NetworkWB(5).Weights;
layers(14).Bias = NetworkWB(5).Bias;
      options = trainingOptions('sgdm','ExecutionEnvironment','gpu',...
          'Shuffle','never',...
          'CheckpointPath','.\Model2',...
          'L2Regularization',reg,...
          'InitialLearnRate',0.01,... 
          'MaxEpochs',epoch, ...
          'Momentum',momentum,...
          'MiniBatchSize',minibatch);
[convnet,traininfo]  = trainNetwork(imtr,categorical(labelstra),layers,options);
%legge tutte le reti nella directory Model2
flist=dir('.\Model2\*.mat');
ii = [flist(:).datenum]; 
[~,ii] = sort(ii);
flist = {flist(ii).name};
Errtemp=100;
netprec=[];
for i=1:length(flist)
     network=load(strcat('.\Model2\',string(flist(i))));
     YVal = classify(network.net,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
     TVal = (categorical(labelsval));
ErrorVa(i) = (sum(categorical(YVal) ~= categorical(TVal))/numel(TVal))*100;
ErrorTr(i)=(sum(100-traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))))/(numel(traininfo.TrainingAccuracy(1:i*fix(dim/minibatch))));
%netprec =network.net;

if(mod(i,pazienza)==0)%earlystopping
if(Errtemp>ErrorVa(i))
    Errtemp=ErrorVa(i);
    netprec =network.net;
else 
    i=i-pazienza;
    break;

end
end
%}
end
figure
plot(pazienza:pazienza:i,traininfo.TrainingLoss(pazienza*fix(dim/minibatch):pazienza*fix(dim/minibatch):i*fix(dim/minibatch)),'c-')
title('Model 2 Loss function')
xlabel('Epoch')
ylabel('Loss')

figure
plot(pazienza:pazienza:i,ErrorTr(pazienza:pazienza:i),'b-',pazienza:pazienza:i,ErrorVa(pazienza:pazienza:i),'g-')
title('Model 2')
xlabel('Epoch')
ylabel('Error(%)')
legend('training error','validation error')

YVal = classify(netprec,imval,'MiniBatchSize',minibatch,'ExecutionEnvironment','gpu')';
confusionMatrix(labelsval,YVal,'Model 2 Validation set');
figure;
perm = randperm(length(YVal));
for i = 1:20
    subplot(4,5,i);
    imshow(imval(:,:,1,perm(i)),[0 255]);
    title(char(YVal(perm(i))));
end
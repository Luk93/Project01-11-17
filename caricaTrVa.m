function [ imtr , labelstra ,imval,labelsval ] =caricaTrVa
images = loadMNISTImages('train-images.idx3-ubyte');
labels = loadMNISTLabels('train-labels.idx1-ubyte');
[images,labels]=cambiaFormato(images,labels);
temp=randperm(length(labels));%shuffle
images=images(:,:,1,temp);
labels=labels(temp);
dim=size(images,4);
perc=0.80;%percentuale training set
 imtr=zeros(28,28,1,int64(dim*perc),'int16');
 imval=zeros(28,28,1,int64(dim*(1-perc)),'int16');
 indici_casuali=randperm(dim);
 val=zeros(1,10);
 labelstra=zeros(1,int64(dim*perc),1,'int8');
 labelsval=zeros(1,int64(dim*(1-perc)),1,'int8');
 y=1;
 p=1;
 for i=1:dim%Stratified Random Sampling
     z=1;
       switch logical(true)
             case (labels(indici_casuali(i)))==0&&val(1)<int64(0.1*dim*(perc))
                 val(1)=val(1)+1;
                 labelstra(y)=labels(indici_casuali(i));
                  for j=1:28
                     for k=1:28
                       imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                       z=z+1;
                     end
                  end
                  y=y+1;
             case (labels(indici_casuali(i)))==1&&val(2)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(2)=val(2)+1;
                  for j=1:28
                     for k=1:28
                       imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                       z=z+1;
                     end
                  end
                  y=y+1;
             case (labels(indici_casuali(i)))==2&&val(3)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(3)=val(3)+1;
                  for j=1:28
                     for k=1:28
                        imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                        z=z+1;
                     end
                  end
                  y=y+1;
             case (labels(indici_casuali(i)))==3&&val(4)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(4)=val(4)+1;
                  for j=1:28
                     for k=1:28
                      imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                      z=z+1;
                     end
                  end
                  y=y+1;
            case (labels(indici_casuali(i)))==4&&val(5)<int64(0.1*dim*(perc))
                labelstra(y)=labels(indici_casuali(i));
                val(5)=val(5)+1;
                 for j=1:28
                     for k=1:28
                       imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                       z=z+1;
                     end
                 end
                 y=y+1;
            case (labels(indici_casuali(i)))==5&&val(6)<int64(0.1*dim*(perc))
                labelstra(y)=labels(indici_casuali(i)); 
                val(6)=val(6)+1;
                 for j=1:28
                     for k=1:28
                      imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                      z=z+1;
                     end
                 end
                 y=y+1;
            case (labels(indici_casuali(i)))==6&&val(7)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(7)=val(7)+1;
                 for j=1:28
                     for k=1:28
                       imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                       z=z+1;
                     end
                 end
                 y=y+1;
            case (labels(indici_casuali(i)))==7&&val(8)<int64(0.1*dim*(perc))
                  labelstra(y)=labels(indici_casuali(i));                
                  val(8)=val(8)+1;
                  for j=1:28
                     for k=1:28
                        imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                        z=z+1;
                     end
                  end
                  y=y+1;
            case (labels(indici_casuali(i)))==8&&val(9)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(9)=val(9)+1;
                  for j=1:28
                     for k=1:28
                        imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                        z=z+1;    
                     end
                  end
                  y=y+1;
            case (labels(indici_casuali(i)))==9&&val(10)<int64(0.1*dim*(perc))
                 labelstra(y)=labels(indici_casuali(i));
                 val(10)=val(10)+1;
                  for j=1:28
                     for k=1:28
                        imtr(k,j,1,y)=images(k,j,1,indici_casuali(i));
                        z=z+1;
                     end
                  end
                  y=y+1;
           otherwise
               labelsval(p)=labels(indici_casuali(i));
                for j=1:28
                     for k=1:28
                       imval(k,j,1,p)=images(k,j,1,indici_casuali(i));
                       
                       z=z+1;
                      
                     end
                end
                p=p+1;
       end
 end
[imval , labelsval]=Bilanciamento( imval , labelsval );%Oversampling
end
Requirement: matlab r2017a

Explanation of files:

-caricaTe it load the test set

-caricaTrVa it load and divide training data in:training set and validation set(in this file,we can do oversampling and stratified random sampling).
-crossValidation performs cross validation of the 3 models
-training_1,training_2,training_3 these file train and test networks 1,2,3
Dataset
Datasets:Dataset_shuffle_SRS_OS or Dataset_SRS_OS or Dataset_SRS ,have been created by caricaTrVa.And dataset dataset_crossvalidationkfold,has been created by this line of code:
images = loadMNISTImages('train-images.idx3-ubyte');
labels = loadMNISTLabels('train-labels.idx1-ubyte');
Procedure of execution for holdout:
-create folder Model1,Model2,Model3 in this directory
-run caricaTrVa or load dataset from Dataset_shuffle_SRS_OS or Dataset_SRS_OS or Dataset_SRS 
-run  training_1.m
 or training_2.m or training_3.m 
-test Test.m
Procedure of execution for Cross Validation K-Fold:
-create folder Model1,Model2,Model3 in this directory
-load dataset from folder: Dataset_crossvalidationKfold
-run CrossValidation.m
In Modello1_deep_learning,Modello2_deep_learning,Modello3_deep_learning, we can find the model trained at PassoX.
Nota:
Before executing files to be sure to have NVIDIA GEFORCE with capability 3.0.If you have it, you can start the procedure of execution otherwise you must change the function defined with argument ExecutionEnvironment from 'ExecutionEnvironment', 'gpu' to 'ExecutionEnvironment' , 'cpu'.
